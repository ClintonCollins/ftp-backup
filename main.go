package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/jlaffaye/ftp"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

type Configuration struct {
	FTPHost       string
	FTPUsername   string
	FTPPassword   string
	FilesToBackup []string
	FTPPath       string
	DaysToKeep    int
}

func ReadConfigurationFile() (*Configuration, error) {
	currentDirectory, err := os.Getwd()
	if err != nil {
		return &Configuration{}, err
	}
	configFile, err := ioutil.ReadFile(currentDirectory + "/config.toml")
	if err != nil {
		log.Fatal(err)
	}
	newConfiguration := Configuration{}
	_, err = toml.Decode(string(configFile), &newConfiguration)
	if err != nil {
		return &newConfiguration, err
	}
	return &newConfiguration, err
}

func main() {
	config, err := ReadConfigurationFile()
	if err != nil {
		log.Fatal(err)
	}
	if !strings.HasSuffix(config.FTPPath, "/") {
		config.FTPPath += "/"
	}
	ftpClient, err := ftp.DialTimeout(config.FTPHost, time.Second*5)
	if err != nil {
		log.Fatal(err)
	}
	err = ftpClient.Login(config.FTPUsername, config.FTPPassword)
	if err != nil {
		log.Fatal(err)
	}

	for _, fileToUpload := range config.FilesToBackup {
		fileExtensionIndex := strings.LastIndex(fileToUpload, ".")
		fileNameWithoutExtension := fileToUpload[:fileExtensionIndex]
		FileNameExtension := fileToUpload[fileExtensionIndex:]
		backupName := fmt.Sprintf("%s-%v%s", fileNameWithoutExtension, time.Now().Format(time.RFC3339), FileNameExtension)
		fmt.Printf("Attempting to upload: %s renamed as %s\n", fileToUpload, backupName)
		fileHandler, err := os.Open(fileToUpload)
		if err != nil {
			fmt.Println(err)
			continue
		}
		err = ftpClient.Stor(config.FTPPath +backupName, fileHandler)
		if err != nil {
			fmt.Println(err)
			continue
		}
		err = fileHandler.Close()
		if err != nil {
			fmt.Println(err)
			continue
		}
		fmt.Printf("Finished uploading %s\n", fileToUpload)
	}

	files, err := ftpClient.List(config.FTPPath)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Checking for files to prune that are %d days old or older.\n", config.DaysToKeep)
	for _, f := range files {
		if f.Name == ".." || f.Name == "." || f.Type != 0 {
			continue
		}
		hoursOld := time.Since(f.Time).Hours()
		daysOld := hoursOld / 24
		if daysOld > float64(config.DaysToKeep) {
			err := ftpClient.Delete(config.FTPPath + f.Name)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println("Successfully deleted file: " + f.Name)
			}
		}
	}
	fmt.Println("Finished pruning files.")
}
